package com.example.hangman_marionavas

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.hangman_marionavas.databinding.ActivityMenuBinding

var difficulty = ""

class MenuActivity : AppCompatActivity() {
    lateinit var binding: ActivityMenuBinding
    lateinit var difficulties: Spinner
    lateinit var playButton: Button
    lateinit var helpButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(supportActionBar!= null)
            supportActionBar?.hide()
        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)

        difficulties = binding.difficulty
        playButton = binding.playButton
        helpButton = binding.helpButton

//        val toast = Toast.makeText(applicationContext, "Intrude difficulty", Toast.LENGTH_SHORT)

        helpButton.setOnClickListener {
            val alertDialog: android.app.AlertDialog? =
                android.app.AlertDialog.Builder(this@MenuActivity).create()
            if (alertDialog != null) {
                alertDialog.setTitle("Instructions")
                alertDialog.setMessage("This is the hangmang game. To win, you must guess a word, the word's length depends on the difficulty you choose. You can do six errors, if you do more, you lose. \nGOOD LUCK!")
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
                alertDialog.show()
            }
        }
        playButton.setOnClickListener {
            val play = Intent(this, GameActivity::class.java)

            difficulty = difficulties.selectedItem.toString()
            startActivity(play)

        }
    }
}