package com.example.hangman_marionavas

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.example.hangman_marionavas.databinding.ActivityGameBinding
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

var fails = 0
var tries = 0
var game = false
var secretWordGame = ""


class GameActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var binding: ActivityGameBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGameBinding.inflate(layoutInflater)
        if(supportActionBar!= null)
            supportActionBar?.hide()
        setContentView(binding.root)

        binding.a.setOnClickListener(this)
        binding.b.setOnClickListener(this)
        binding.c.setOnClickListener(this)
        binding.d.setOnClickListener(this)
        binding.e.setOnClickListener(this)
        binding.f.setOnClickListener(this)
        binding.g.setOnClickListener(this)
        binding.h.setOnClickListener(this)
        binding.i.setOnClickListener(this)
        binding.j.setOnClickListener(this)
        binding.k.setOnClickListener(this)
        binding.l.setOnClickListener(this)
        binding.m.setOnClickListener(this)
        binding.n.setOnClickListener(this)
        binding.ny.setOnClickListener(this)
        binding.o.setOnClickListener(this)
        binding.p.setOnClickListener(this)
        binding.q.setOnClickListener(this)
        binding.r.setOnClickListener(this)
        binding.s.setOnClickListener(this)
        binding.t.setOnClickListener(this)
        binding.u.setOnClickListener(this)
        binding.v.setOnClickListener(this)
        binding.w.setOnClickListener(this)
        binding.x.setOnClickListener(this)
        binding.y.setOnClickListener(this)
        binding.z.setOnClickListener(this)

        var secretWordView = ""
        val wordsEasy = listOf("perla", "tenaz" )
        val wordsMedium = listOf("carmin", "hilera", "sonata", "gotera", "ejemplo")
        val wordsHard = listOf("tortilla", "lagartija")

        when(difficulty){
            "Easy" -> secretWordGame = wordsEasy.random()
            "Medium" ->secretWordGame = wordsMedium.random()
            "Hard" ->secretWordGame = wordsHard.random()

        }

        tries = 0
        fails = 0
//        secretWordGame="alberto"

        val secretWordLength: Int = secretWordGame.length
        repeat(secretWordLength){
            secretWordView += "_ "
        }


        binding.wordTextView.text = secretWordView

        binding.lettersUsedTextView.text= "Used Letters:"

    }


    override fun onClick(p0: View?) {
        var hangmanView = binding.imageView
        var secretWord = secretWordGame.uppercase()
        val button=p0 as Button
        val text=button.text
        val textId=button.id
        p0.isEnabled = false
        checkWord(text.toString())
        var secretWordView= binding.wordTextView.text.toString().replace(" ", "")
        if(secretWord==secretWordView){
            game = true
            val result = Intent(this, ResultActivity::class.java)
            startActivity(result)
        }
        else{
            changeImage(fails,hangmanView)
            if(fails==6){
                val handler = Handler(Looper.getMainLooper())
                    handler.postDelayed({
                        changeImage(7,hangmanView)
                    }, 600)
                    handler.postDelayed({
                        val result = Intent(this, ResultActivity::class.java)
                        startActivity(result)
                                        }, 600)
            }
        }

        tries += 1
    }

    fun checkWord(letter:String){

        val secretWord = secretWordGame.uppercase()
        val secretWordView= binding.wordTextView.text.split(" ").toMutableList()
        var usedLetters = binding.lettersUsedTextView.text.toString()
        var newSecretWordView = ""
        var letterIn = false

        for(i in 0 .. secretWord.lastIndex){
            if(letter == secretWord[i].toString()){
                secretWordView[i]= secretWord[i].toString()
                letterIn = true
            }
        }

        for(j in secretWordView){
            newSecretWordView+="$j "
        }

        binding.wordTextView.text = newSecretWordView

        if(!letterIn){
            fails += 1
        }

        usedLetters += "$letter, "
        binding.lettersUsedTextView.text = usedLetters
    }

    fun changeImage(currentStage: Int, hangmanView: ImageView){
        val images: Array<Int> = arrayOf(R.drawable.hangman, R.drawable.hangman1, R.drawable.hangman2, R.drawable.hangman3,
            R.drawable.hangman4, R.drawable.hangman5, R.drawable.hangman6, R.drawable.hangman7)
        hangmanView.setImageResource(images[currentStage])

    }

}