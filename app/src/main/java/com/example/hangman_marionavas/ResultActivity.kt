package com.example.hangman_marionavas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import com.example.hangman_marionavas.databinding.ActivityResultBinding

class ResultActivity : AppCompatActivity() {
    lateinit var binding: ActivityResultBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityResultBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle: Bundle? = intent.extras


        if(game){
            println("congrats")
            binding.gameOver.visibility = INVISIBLE
            binding.noSucceed.visibility = INVISIBLE
            binding.congrats.visibility = VISIBLE
            binding.succeed.visibility = VISIBLE

        }else{
            println("you lost")
            binding.gameOver.visibility = VISIBLE
            binding.noSucceed.visibility = VISIBLE
            binding.congrats.visibility = INVISIBLE
            binding.succeed.visibility = INVISIBLE
        }
        binding.numAttempts.text = tries.toString()



        val play = Intent(this, GameActivity::class.java)
        val menu = Intent(this, MenuActivity::class.java)


        binding.button.setOnClickListener(){
            startActivity(play)
        }
        binding.button2.setOnClickListener(){
            startActivity(menu)
        }
    }
}